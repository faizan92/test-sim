<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Dashboard Template for Bootstrap</title>

        <!-- Bootstrap core CSS -->

        <link href="<?php echo base_url('css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/style.css'); ?>" rel="stylesheet" type="text/css" />
        <!--<link href="<?php // echo base_url('css/image-picker.css'); ?>" rel="stylesheet" type="text/css" />-->

        <!-- Custom styles for this template -->
        <link href="dashboard.css" rel="stylesheet">

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="<?php echo base_url('js/ie-emulation-modes-warning.js'); ?>"></script>

        <!--jquery link for bulk image upload-->
        <script type="text/javascript" src="<?php echo base_url('js/jquery-1.8.3.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('js/jquery.form.js'); ?>"></script>
        <!--<script type="text/javascript" src="<?php // echo base_url('js/image-picker.js'); ?>"></script>-->

        <!--jquery select opton-->


        <script type="text/javascript">
            $(document).ready(function () {
                $('#images').on('change', function () {
                    $('#multiple_upload_form').ajaxForm({
                        target: '#images_preview',
                        beforeSubmit: function (e) {
                            $('.uploading').show();
                        },
                        success: function (e) {
                            $('.uploading').hide();
                        },
                        error: function (e) {
                        }
                    }).submit();
                });
            });
        </script>


    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">SMART INVENTORY Management System.......</a>
                </div>

            </div>
        </nav>

        <div class="container-fluid">


            <div class="col-sm-12 col-md-12" style="margin-top: 2%;">
                <h1 class="page-header">Image Uploads</h1>
                <div>
                    <form method="post" name="multiple_upload_form" id="multiple_upload_form" enctype="multipart/form-data" action="<?php echo site_url('image_con/image_upload'); ?>">
                        <input type="hidden" name="image_form_submit" value="1"/>
                        <label>Choose Image</label>
                        <input type="file" name="images[]" id="images" multiple >
                        <div class="uploading none">
                            <label>&nbsp;</label>
                            <img src="<?php echo base_url('img/uploading.gif'); ?>"/>
                        </div>
                    </form>
                </div>



                <!--<h2 class="sub-header">Image inventory Manage System</h2>-->
                <div class="row">
                    <div class="col-md-4">
                        <h1>Select Category<hr></h1>
                        <div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Select Vehicle Category</label><br>
                                <select class="form-control">
                                    <option selected="selected">Select Maker</option>
                                    <option>Toyota</option>
                                    <option>Mazda</option>
                                    <option>Tesla</option>
                                    <option>Honda</option>
                                    <option>BMW</option>
                                    <option>Audi</option>
                                    <option>Benz</option>
                                </select>
                               
                            </div>
                             
                        </div>
                    </div>
                    <div class="col-md-8">
     
<!--    <div class="picker">
      <select multiple="multiple" class="image-picker show-html">
        <option data-img-src='http://placekitten.com/220/200' value='1'>Cute Kitten 1</option>
        <option data-img-src='http://placekitten.com/180/200' value='2'>Cute Kitten 2</option>
        <option data-img-src='http://placekitten.com/130/200' value='3'>Cute Kitten 3</option>
        <option data-img-src='http://placekitten.com/270/200' value='4'>Cute Kitten 4</option>
      </select>
    </div>-->
                        <div class="gallery" id="images_preview">

                        </div>
                       
                       

                    </div>

                </div>
            </div>
        </div>

<!--        <script>
            $("select").imagepicker();
        </script>-->
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
<!--        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="<?php // echo base_url('js/bootstrap.min.js')       ?>"></script>
         Just to make our placeholder images work. Don't actually copy the next line! 
        <script src="<?php // echo base_url('js/vendor/holder.min.js')       ?>"></script>
         IE10 viewport hack for Surface/desktop Windows 8 bug 
        <script src="<?php // echo base_url('js/ie10-viewport-bug-workaround.js')       ?>"></script>-->
    </body>
</html>
